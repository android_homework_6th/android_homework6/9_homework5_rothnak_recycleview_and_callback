package com.somee.rothnaksomang.homework5_recycleview_with_callback.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.somee.rothnaksomang.homework5_recycleview_with_callback.R;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.addInfoDialog.AddInfoDialog;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.callback.UniversityCallback;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.entity.University;
import java.util.ArrayList;

public class UniversityAdapter extends RecyclerView.Adapter<UniversityAdapter.UniversityViewHolder>{

    private Context context;
    private ArrayList<University> universities;
    private LayoutInflater inflater;
    private UniversityCallback universityCallback;
    Uri uriProfile;

    public UniversityAdapter(Context context,ArrayList<University> list){
        this.context=context;
        this.universities=list;
        inflater=LayoutInflater.from(context);
        universityCallback= (UniversityCallback) context;
    }

    @NonNull
    @Override
    public UniversityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.layout_card_item,null);
        return new UniversityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UniversityViewHolder universityViewHolder, int i) {
        final University university=universities.get(i);
        universityViewHolder.ivProfileBig.setImageURI(Uri.parse(university.getProfile()));
        universityViewHolder.ivProfileSmall.setImageURI(Uri.parse(university.getProfile()));
        universityViewHolder.tvName.setText(university.getName());
        universityViewHolder.tvPhone.setText(university.getPhone());
        universityViewHolder.tvWebsite.setText(university.getWebsite());
        universityViewHolder.tvMail.setText(university.getMail());
        universityViewHolder.tvAddress.setText(university.getAddress());

        universityViewHolder.cvUniversity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlWebiste="http://"+university.getWebsite();
                Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(urlWebiste));
                context.startActivity(intent);
            }
        });

        universityViewHolder.cvUniversity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Update University Information");
                builder.setMessage("Do you want to update the information of this university?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context,"Yes",Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder= new AlertDialog.Builder(context);

                        View view=LayoutInflater.from(context).inflate(R.layout.layout_dialog_add,null);
                        builder.setTitle("Update Information of University");

                        builder.setView(view);
                        builder.show();

                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context,"No",Toast.LENGTH_SHORT).show();

                    }
                });
                builder.show();

                return false;
            }
        });
    }
    @Override
    public int getItemCount() {
        return universities.size();
    }

    class UniversityViewHolder extends RecyclerView.ViewHolder{
        TextView tvName,tvPhone,tvWebsite,tvMail,tvAddress;
        ImageView ivProfileBig,ivProfileSmall;
        Button btnDelete;
        CardView cvUniversity;

        public UniversityViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvPhone=itemView.findViewById(R.id.tv_call);
            tvWebsite=itemView.findViewById(R.id.tv_web);
            tvMail=itemView.findViewById(R.id.tv_mail);
            tvAddress=itemView.findViewById(R.id.tv_address);
            ivProfileBig=itemView.findViewById(R.id.iv_profile_big);
            ivProfileSmall=itemView.findViewById(R.id.iv_profile_small);
            btnDelete=itemView.findViewById(R.id.btn_delete);
            cvUniversity=itemView.findViewById(R.id.cv_item_university);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    University university=universities.get(getAdapterPosition());
                    universityCallback.delete(university,getAdapterPosition());
                }
            });
        }

    }


}
