package com.somee.rothnaksomang.homework5_recycleview_with_callback.addInfoDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.R;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.callback.UniversityCallback;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.entity.University;


public class AddInfoDialog extends DialogFragment {

    private UniversityCallback.AddNewInfo addNewInfo;
    private Uri profile=null;
    ImageView ivProfileBig,ivProfileSmall;

    public  AddInfoDialog(){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        addNewInfo= (UniversityCallback.AddNewInfo) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder= new AlertDialog.Builder(getContext());

        View view=LayoutInflater.from(getContext()).inflate(R.layout.layout_dialog_add,null);
        builder.setTitle("Add Information of University");
        builder.setView(view);

        final UniversityViewHolder universityViewHolder=new UniversityViewHolder(view);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name=universityViewHolder.etName.getText().toString();
                String phone=universityViewHolder.etPhone.getText().toString();
                String website=universityViewHolder.etWebsite.getText().toString();
                String mail=universityViewHolder.etMail.getText().toString();
                String address=universityViewHolder.etAddress.getText().toString();
//                int profild=universityViewHolder.ivProfileBig.getDrawable();

                University university=new University(name,phone,website,mail,address,profile.toString());
                addNewInfo.getUniversity(university);
            }
        });

        return builder.create();
    }
    class UniversityViewHolder {
        EditText etName,etPhone,etWebsite,etMail,etAddress;
        Button btnPickProfile;
        int GALLERY=1;

        public UniversityViewHolder(View view){
            etName=view.findViewById(R.id.et_name);
            etPhone=view.findViewById(R.id.et_phone);
            etWebsite=view.findViewById(R.id.et_web);
            etMail=view.findViewById(R.id.et_mail);
            etAddress=view.findViewById(R.id.et_address);
            ivProfileBig=view.findViewById(R.id.iv_profile_big);
            ivProfileSmall=view.findViewById(R.id.iv_profile_small);
            btnPickProfile=view.findViewById(R.id.btn_pick_profile);


            btnPickProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getImageFromGallery();
                }
            });
        }
        public void getImageFromGallery(){
            Intent i=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i,GALLERY);
        }


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==1){
            return;
        }
        if (requestCode==1 & data!=null){
            profile=data.getData();
            ivProfileBig.setImageURI(profile);
            ivProfileSmall.setImageURI(profile);
        }
    }

}
