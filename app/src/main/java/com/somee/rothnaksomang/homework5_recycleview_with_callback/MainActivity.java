package com.somee.rothnaksomang.homework5_recycleview_with_callback;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.somee.rothnaksomang.homework5_recycleview_with_callback.adapter.UniversityAdapter;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.addInfoDialog.AddInfoDialog;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.callback.UniversityCallback;
import com.somee.rothnaksomang.homework5_recycleview_with_callback.entity.University;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements UniversityCallback,UniversityCallback.AddNewInfo{

    ArrayList<University> universities;
    RecyclerView recyclerView;
    UniversityAdapter universityAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        universities=new ArrayList<>();

        recyclerView=findViewById(R.id.rv_all_item_vertical);
        universityAdapter=new UniversityAdapter(this,universities);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(universityAdapter);

        createData();


    }

    public void createData(){
//        for(int i=1;i<=10;i++){
//            universities.add(new University("RUPP","098","rupp.com","rupp@gmail","st122",R.drawable.black_pink));
//        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.layout_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if (id == R.id.mnAdd) {
            AddInfoDialog addInfoDialog=new AddInfoDialog();
            addInfoDialog.show(getSupportFragmentManager(),"Add a new University");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void delete(University university, int position) {
        universities.remove(university);
        universityAdapter.notifyItemRemoved(position);
    }

    @Override
    public void getUniversity(University university) {
        universities.add(0,university);
        universityAdapter.notifyItemChanged(0);
    }
}
