package com.somee.rothnaksomang.homework5_recycleview_with_callback.entity;

public class University {
    private String name;
    private String phone;
    private String website;
    private String mail;
    private String address;
    private String profile;

    public University(){

    }

    public University(String name, String phone, String website, String mail, String address, String profile) {
        this.name = name;
        this.phone = phone;
        this.website = website;
        this.mail = mail;
        this.address = address;
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
